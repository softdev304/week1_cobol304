       IDENTIFICATION DIVISION.
       PROGRAM-ID. PLUS-TWO-NUMBER.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01  NUM   PIC 9(8).
       01  NUM2  PIC 9(8).
       01  RESULT   PIC 9(8).
       PROCEDURE DIVISION.
       MAIN-PROCEDURE.
           DISPLAY "Please input number 1: " WITH NO ADVANCING 
           ACCEPT NUM 
           DISPLAY "Please input number 2: " WITH NO ADVANCING
           ACCEPT NUM2
           COMPUTE RESULT = NUM + NUM2
           DISPLAY "RESULT = " RESULT. 
